# Consultora de clima

En este proyecto creamos una aplicación del clima donde el usuario puede escribir la ciudad que quiere consultar y seleccionar el país a la cual pertenece. 

El mismo es el proyecto 5 de 10 del curso de Vue.js 3 tomado en Udemy. El propósito de estos es formarme con las bases fundamentales de esta tecnologia aplicando de manera práctica a proyectos realistas.

- Se encuentra levantado en https://consultora-de-clima.vercel.app/
