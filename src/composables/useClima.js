import axios from "axios"
import {ref, computed} from 'vue'

export default function useClima() {

    const clima = ref({})
    const cargando = ref('')
    const error = ref('')

    const mostrarClima = computed(()=>{
        return Object.values(clima.value).length !== 0
    })

    const obtenerClima = async ({ciudad, pais}) => {
        const key = import.meta.env.VITE_API_KEY
        cargando.value = true
        clima.value = {}
        error.value = ''
        try {
            const url = `https://api.openweathermap.org/geo/1.0/direct?q=${ciudad},${pais}&limit=1&appid=${key}`
            const {data} = await axios(url)
            const {lat, lon} = data[0]
            const urlClima = `https://api.openweathermap.org/data/2.5/weather?lat=${lat}&lon=${lon}&appid=${key}&units=metric`
            const {data: resultadoClima} = await axios(urlClima)
            clima.value = resultadoClima
        } catch {
            error.value = 'Ciudad no encontrada'
        } finally {
            cargando.value = false
        }
    }

    const altaTemperatura = (temperatura) => {
        return (temperatura > 30.0)
    }
    
    return {
        obtenerClima,
        clima,
        mostrarClima,
        altaTemperatura, 
        cargando,
        error
    }
}